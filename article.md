---
title: "Yusra plave o život: Třetí díl seriálu, který od Českého rozhlasu převzala i BBC"
perex: "Unikátní fotografická a zvuková reportáž z putování syrské plavkyně Yusry Mardini z rozbombardovaného Damašku na olympijské hry do Ria de Janeira."
description: "Unikátní fotografická a zvuková reportáž z putování syrské plavkyně Yusry Mardini z rozbombardovaného Damašku na olympijské hry do Ria de Janeira."
authors: ["Magdalena Sodomková", "Hiên Lâm Duc", "Brit Jensen"]
published: "14. září 2016"
socialimg: https://interaktivni.rozhlas.cz/yusra/media/dil3.jpg
url: yusra
---

<iframe frameborder="0" scrolling="no" src="https://samizdat.pageflow.io/yusra-mobile-3-dil/embed"></iframe>
